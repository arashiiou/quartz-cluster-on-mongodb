package idv.kevin.quartzclusteronmongodb.config;

import idv.kevin.quartzclusteronmongodb.job.ShowTimeJob;
import org.quartz.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class QuartzConfig {


	@Bean
	public JobDetail showTimeJobDetail(){
		return JobBuilder.newJob(ShowTimeJob.class)
			.withIdentity("time_announcement", "time_announcement")
			.storeDurably()
			.build();
	}

	@Bean
	public Trigger showTimeTrigger(){
		return TriggerBuilder.newTrigger()
			.forJob(showTimeJobDetail())
			.withIdentity("time_announcement", "time_announcement")
			.withSchedule(CronScheduleBuilder.cronSchedule("0/30 * * ? * * *"))
			.build();
	}
}
