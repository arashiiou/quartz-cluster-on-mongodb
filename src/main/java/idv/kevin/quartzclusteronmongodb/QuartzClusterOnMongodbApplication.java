package idv.kevin.quartzclusteronmongodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuartzClusterOnMongodbApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuartzClusterOnMongodbApplication.class, args);
	}

}
