### Introduction
Quartz-cluster does not support MongoDB.<br>
So, there has third-part library as below.
https://github.com/michaelklishin/quartz-mongodb

using spring boot + gradle to build our project

### Run Test

using gradle bootJar to build two or more jar file
   
start up all of your jar file.

you will see only 1 node executes scheduler like below every 30 second

```
2021-03-15 17:39:00.015  INFO 67527 --- [eduler_Worker-1] i.k.q.job.ShowTimeJob                    : 
2021-03-15 17:39:00.015  INFO 67527 --- [eduler_Worker-1] i.k.q.job.ShowTimeJob                    : ====>  2021-03-15T17:39:00.014990 
2021-03-15 17:39:00.015  INFO 67527 --- [eduler_Worker-1] i.k.q.job.ShowTimeJob                    : 
2021-03-15 17:39:00.020  INFO 67527 --- [eduler_Worker-1] c.n.quartz.mongodb.dao.LocksDao          : Removing trigger lock time_announcement.time_announcement.NON_CLUSTERED
2021-03-15 17:39:00.024  INFO 67527 --- [eduler_Worker-1] c.n.quartz.mongodb.dao.LocksDao          : Trigger lock time_announcement.time_announcement.NON_CLUSTERED removed.
2021-03-15 17:39:00.034  INFO 67527 --- [SchedulerThread] c.n.quartz.mongodb.dao.LocksDao          : Inserting lock for trigger time_announcement.time_announcement
2021-03-15 17:39:00.038  INFO 67527 --- [SchedulerThread] c.n.quartz.mongodb.TriggerRunner         : Acquired trigger: time_announcement.time_announcement
```


